import daytona from "daytona";
import { renderFile } from "ejs";
import cors from "cors";

const app = daytona();
app.use(cors());
app.use("/static", daytona.static("./static"));

app.engine("ejs", renderFile);
app.set("view engine", "ejs");

app.get("/", (req, res) => res.redirect("/w7") );
app.get("/w1", (req, res) => res.render("w1/index", { week: 1 }));
app.get("/w2", (req, res) => res.render("w2/index", { week: 2 }));
app.get("/w3", (req, res) => res.render("w3/index", { week: 3 }));
app.get("/w7", (req, res) => res.render("w7/index", { week: 7 }));

app.listen(4321);
